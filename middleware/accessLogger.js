/* HTTP Access Logger Middleware */
// Dependencies
var morgan = require('morgan')
var FileStreamRotator = require('file-stream-rotator')
var fs = require('fs')

var logDirectory = __dirname + '/log'
// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)
// create a rotating write stream
var accessLogStream = FileStreamRotator.getStream({
  filename: logDirectory + '/access-%DATE%.log',
  frequency: 'daily',
  verbose: false
})

// setup the logger
var accessLogger = morgan('combined', {
  stream: accessLogStream,
  skip: function (req, res) { return res.statusCode < 400 } // only log error
})

module.exports = accessLogger
