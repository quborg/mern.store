// Dependencies
var express = require('express')
  , httpResponseHeader = express()

httpResponseHeader.use(function(req, res, next) {
  res.set('X-Powered-By', 'Mern Store')
  next()
})

module.exports = httpResponseHeader
