// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var productSchema = new mongoose.Schema({
  sku: String,
  name: String,
  price: Number
});

// Return model
module.exports = restful.model('Product', productSchema);
