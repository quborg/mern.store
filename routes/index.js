var express = require('express')
  , routes = express.Router()

/* GET home page. */
routes.get('/', function(req, res, next) {
  res.send('MERN API')
})
routes.use('/api', require('./product'))

module.exports = routes
