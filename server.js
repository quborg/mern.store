// Dependencies
var express = require('express')
  , mongoose = require('mongoose')
  , bodyParser = require('body-parser')
  , app = express()
  , routes = require('./routes/index')

// MongoDB
mongoose.connect('mongodb://localhost/mern_store')

// Express
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

// Routes
app.use('/', routes)
// app.use('/api', routes)

// Start server
var server = app.listen(3000, function() {
  console.log('API is running on port ' + server.address().port)
})
